<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="centro">
<html>
<body>
<xsl:for-each select="curso">
	<table border="2" bgcolor="#99CCFF" width="100%">
		<tr>
			<th>NOMBRE</th>
			<th>HORAS</th>
			<th>COSTO</th>
			<th>REQUIERE</th>
			<th>RECOMIENDA</th>
		</tr>
		<tr>
			<td>
			<xsl:value-of select= "nombre"/>
			</td>
			<td>
			<xsl:value-of select= "horas"/>
			</td>
			<td>
			<xsl:value-of select= "costo"/>
			</td>
			<td>
			<xsl:value-of select= "requiere/@cursos"/>
			</td>
			<td>
			<xsl:value-of select= "recomienda/@cursos"/>
			</td>		
		</tr>
	</table>
</xsl:for-each>
</body>
</html>
</xsl:template>
</xsl:stylesheet>