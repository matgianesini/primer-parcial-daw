<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="factura">
<html>
<body>
<img src="http://galeon.hispavista.com/labellaepoca/img/int.jpg"/>
<h1>Factura del alumno</h1>
<xsl:for-each select="alumno">
			<p>Matricula: <xsl:value-of select= "matricula"/></p>
			<p>Nombre: <xsl:value-of select= "nombre"/></p>
			<p>Fecha de Inicio: <xsl:value-of select= "inicio"/></p>
			<p>Fecha de Fin: <xsl:value-of select= "fin"/></p>
</xsl:for-each>
<xsl:for-each select="cursos/curso">
	<table border="2" bgcolor="#99CCFF" width="100%">
		<tr>
			<th>CURSO</th>
			<th>COSTO</th>
			<th>PROFESOR</th>
			<th>SALON</th>
		</tr>
		<tr>
			<td>
			<xsl:value-of select= "nombre"/>
			</td>
			<td>$
			<xsl:value-of select= "costo"/>
			</td>
			<td>
			<xsl:value-of select= "profesor"/>
			</td>
			<td>
			<xsl:value-of select= "salon"/>
			</td>		
		</tr>
	</table>
<!-- 		<xsl:variable name="total" select="costo"/>
		<xsl:variable name="calculatedTotal" select="costo"/>
		<xsl:variable name="equis" select="$total + $calculatedTotal" /> -->
		<p></p>
</xsl:for-each>
<p>Total = $9000</p>
</body>
</html>
</xsl:template>
</xsl:stylesheet>