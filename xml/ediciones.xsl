<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="centro">
<html>
<body>
<xsl:for-each select="edicion">
	<table border="2" bgcolor="#99CCFF" width="100%">
		<tr>
			<th>CURSO</th>
			<th>PROFESOR</th>
			<th>INICIO</th>
			<th>FIN</th>
			<th>ALUMNOS</th>
		</tr>
		<tr>
			<td>
			<xsl:value-of select="@curso"/>
			</td>
			<td>
			<xsl:value-of select="@profesor"/>
			</td>
			<td>
			<xsl:value-of select= "inicio"/>
			</td>
			<td>
			<xsl:value-of select= "fin"/>
			</td>
			<td>
			<xsl:value-of select= "@alumnos"/>
			</td>	
		</tr>
	</table>
</xsl:for-each>
</body>
</html>
</xsl:template>
</xsl:stylesheet>