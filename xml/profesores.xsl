<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="centro">
<html>
<body>
<xsl:for-each select="profesor">
	<table border="2" bgcolor="#99CCFF" width="100%">
		<tr>
			<th>NOMBRE</th>
			<th>EMPLEADO</th>
			<th>RFC</th>
			<th>ANTIGUEDAD</th>
			<th>SALARIO</th>
		</tr>
		<tr>
			<td>
			<xsl:value-of select= "nombre"/>
			</td>
			<td>
			<xsl:value-of select= "@empleado"/>
			</td>
			<td>
			<xsl:value-of select= "rfc"/>
			</td>
			<td>
			<xsl:value-of select= "antiguedad"/>
			</td>
			<td>
			<xsl:value-of select= "salario"/>
			</td>		
		</tr>
	</table>
</xsl:for-each>
</body>
</html>
</xsl:template>
</xsl:stylesheet>