CREATE TABLE Profesores
(
ProfesorID int NOT NULL AUTO_INCREMENT,
LastNamep varchar(255),
FirstNamep varchar(255),
RFC varchar(255),
Antiguedad date,
Salario int,
Imagenp varchar(255),
PRIMARY KEY (ProfesorID)
);

CREATE TABLE Alumnos
(
AlumnoID int NOT NULL AUTO_INCREMENT,
LastName varchar(255),
FirstName varchar(255),
Matricula varchar(20),
Promedio int,
Imagen varchar(255),
PRIMARY KEY (AlumnoID)
);

CREATE TABLE Cursos
(
ClaseID int NOT NULL AUTO_INCREMENT,
Nombre varchar(255),
Salon varchar(10),
Costo int,
Horas int,
ProfesorID int,
PRIMARY KEY (ClaseID),
FOREIGN KEY (ProfesorID) REFERENCES Profesores(ProfesorID)
);

CREATE TABLE Inscrito
(
InscripcionID int NOT NULL AUTO_INCREMENT,
Calificacion int,
ClaseID int,
AlumnoID int,
PRIMARY KEY (InscripcionID),
FOREIGN KEY (ClaseID) REFERENCES Cursos(ClaseID),
FOREIGN KEY (AlumnoID) REFERENCES Alumnos(AlumnoID)
);