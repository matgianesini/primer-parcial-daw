INSERT INTO Profesores (LastNamep,FirstNamep,RFC,Antiguedad,Salario,Imagenp) VALUES
('Lopez','Javier','LOSJ790205','03/01/02','200','http://i.imgur.com/HY808EG.jpg'),
('Jimenez','Luis','JIML800406','04/02/03','300','http://i.imgur.com/HY808EG.jpg'),
('Garcia','Juan','GARFJ760305','05/03/04','320','http://i.imgur.com/HY808EG.jpg'),
('Ruiz','Jose','RUMJ830406','05/03/04','280','http://i.imgur.com/HY808EG.jpg'),
('Lios','Armando','LIAR730506','07/04/09','150','http://i.imgur.com/HY808EG.jpg');

INSERT INTO Alumnos (LastName,FirstName,Matricula,Promedio,Imagen) VALUES
('Rodriguez','Matias','A01014802','9','http://i.imgur.com/a4ZxA5u.jpg'),
('Mena','Manuel','A01017984','9','http://i.imgur.com/a4ZxA5u.jpg'),
('Radin','Mauricio','A01014563','8','http://i.imgur.com/a4ZxA5u.jpg'),
('Carpio','Juan','A0102154','7','http://i.imgur.com/a4ZxA5u.jpg'),
('Cardenas','Susana','A01014202','8','http://i.imgur.com/a4ZxA5u.jpg');

INSERT INTO Cursos (Nombre,Salon,Costo,Horas,ProfesorID) VALUES
('Tecnico en Hardware','1305','5000','100','2'),
('Introduccion a la Programación','1405','4000','50','1'),
('Programacion Avanzada','1205','10000','100','1'),
('Bases de Datos','1105','3000','50','3'),
('Desarrollo Web con PHP/MySQL','1005','9500','100','4');

INSERT INTO Inscrito (Calificacion,ClaseID,AlumnoID) VALUES
('10','1','2'),
('9','2','1'),
('8','3','2'),
('7','4','3'),
('6','4','4'),
('7','5','3'),
('8','3','2'),
('9','1','1'),
('10','2','5'),
('9','3','5'),
('8','1','4');
