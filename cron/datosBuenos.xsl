<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">	<!-- match all of note -->
<html>				<!-- insert html tags  -->
<body>
<xsl:for-each select="Boleta">
 <table border="2" bgcolor="yellow"> 
  <tr> <!-- insert table headings --> 
    <th>Apellido</th>
    <th>Nombre</th> 
    <th>Matricula</th>
    <th>Promedio</th>
    <th>Foto</th>
    <th>Clase</th>
    <th>Salon</th>
    <th>Calificacion</th>
    <th>Apellidop</th>
    <th>Nombrep</th>
    <th>RFC</th>
    <th>Imagenp</th>
  </tr> 
  <tr> <!-- get values from note-->
	<td>
	<xsl:value-of select= "Apellido"/>
	</td>
	<td>
	<xsl:value-of select="Nombre"/>
	</td>
	<td> 
	<xsl:value-of select="Matricula"/>
	</td>
	<td> 
	<xsl:value-of select="Promedio"/>
	</td> 	
	<td> 
	<xsl:value-of select="Foto"/>
	</td>	
	<td> 
	<xsl:value-of select="Clase"/>
	</td> 	
	<td> 
	<xsl:value-of select="Salon"/>
	</td> 
	<td> 
	<xsl:value-of select="Calificacion"/>
	</td>	
	<td> 
	<xsl:value-of select="Apellidop"/>
	</td>	
	<td> 
	<xsl:value-of select="Nombrep"/>
	</td>	
	<td> 
	<xsl:value-of select="RFC"/>
	</td>		
	<td> 
	<xsl:value-of select="Imagenp"/>
	</td> 
  </tr>
 </table>
 <hr/>
 <hr/>
</xsl:for-each>   <!-- close all wffs -->
</body>
</html>
</xsl:template>
</xsl:stylesheet>